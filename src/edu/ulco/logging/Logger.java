package edu.ulco.logging;
import java.util.ArrayList;
import java.util.List;

public class Logger implements Subject{
    /* niveaux de log possible: */
    public static int DEBUG = -1; // tous les messages sont loggés
    public static int INFO = 0; // tous les messages sont loggés, sauf ceux de débug
    public static int WARNING = 1; // seuls les messages de warning et d'erreur sont loggés
    public static int ERROR = 2; // seuls les messages d'erreur sont loggés
    private String name;
    private int mode;
    private List<Observer> messages = new ArrayList<Observer>();

    public Logger(String name) {
        this(name, ERROR);
    }
    public Logger(String name, int mode) {
        this.name = name;
        this.mode = mode;
    }
    public String getName(){
        return name;
    }
    public int getMode() {
        return mode;
    }
    public List<Observer> getMessages() {
        return messages;
    }
    public void log(int level, String text){
        log(level, null, null, text);
    }
    public void log(int level, String sourceClass, String sourceMethod, String text) {
        if (level >= this.mode) {
            Message msg = new Message(level, sourceClass, sourceMethod, text);
            subscribe(msg);
            update();
        }
    }

    @Override
    public void subscribe(Observer o) {
        messages.add(o);
    }

    @Override
    public void unsubscribe(Observer o) {
        messages.remove(o);
    }

    @Override
    public void update() {
        for (Observer o: messages) {
            o.update();
        }
    }
}
