package edu.ulco.logging;

public interface Observer {
    void update();
}
