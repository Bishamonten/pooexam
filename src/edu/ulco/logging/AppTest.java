package edu.ulco.logging;

public class AppTest {
    public static void main(String[] args) {
        Logger logger = new Logger("logTest", Logger.DEBUG);
        logger.log(Logger.INFO, "information");
        logger.log(Logger.DEBUG, "message pour faciliter le debugage");
        logger.log(Logger.ERROR, "message d'erreur");
        logger.log(Logger.WARNING, "avertissement");
    }
}