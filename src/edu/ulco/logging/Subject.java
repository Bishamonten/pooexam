package edu.ulco.logging;

public interface Subject {
    void subscribe(Observer o);
    void unsubscribe(Observer o);
    void update();
}
