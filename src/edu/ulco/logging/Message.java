package edu.ulco.logging;
public class Message implements Observer{
    private int level;
    private String sourceClass;
    private String sourceMethod;
    private String text;
    public Message(int level, String sourceClass, String sourceMethod, String text) {
        this.level = level;
        this.sourceClass = sourceClass;
        this.sourceMethod = sourceMethod;
        this.text = text;
    }
    public int getLevel() {
        return level;
    }
    public String getSourceClass() {
        return sourceClass;
    }
    public String getSourceMethod() {
        return sourceMethod;
    }
    public String getText() {
        return text;
    }
    @Override
    public String toString() {
        return level + " : " + text;
    }

    @Override
    public void update() {
        System.out.println("Source class: " + sourceClass + " Source method: " + sourceMethod + " Text: " + text);
    }
}